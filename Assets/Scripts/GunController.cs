﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    public bool isPlayer = false;

    public string bulletName = "Bullet";
    public ParticleSystem muzzleFlash;

    public GameObject laser;
    public GameObject laser2;

    Rigidbody body;

    void Awake()
    {
        body = GetComponentInParent<Rigidbody>();
        if(laser != null)
        {
            laser.SetActive(false);
            laser2.SetActive(false);
        }
        
    }

    private void Update()
    {
        Fire();
    }

    public void Fire()
    {
        if (isPlayer)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameObject spawner = Spawner.Spawn(bulletName);
                spawner.transform.position = transform.position;
                spawner.transform.rotation = transform.rotation;
                spawner.SetActive(true);
                BulletController b = spawner.GetComponent<BulletController>();
                Vector3 v = (body == null) ? Vector3.zero : body.velocity;
                b.Fire(transform.forward, body.velocity);
                muzzleFlash.Play();
                AudioManager.Play("GunSound");
            }
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                laser.SetActive(true);
                laser2.SetActive(true);
            }
            else if (Input.GetKeyUp(KeyCode.Alpha1))
            {
                laser.SetActive(false);
                laser2.SetActive(false);
            }
            
        }
    }

    public void EnemyFire()
    {
        {
            GameObject bullet = Spawner.Spawn(bulletName);
            if (bullet == null)
            {
                return;
            }
            bullet.transform.position = transform.position;
            bullet.transform.rotation = transform.rotation;
            bullet.SetActive(true);
            BulletController b = bullet.GetComponent<BulletController>();
            Vector3 v = (body == null) ? Vector3.zero : body.velocity;
            b.Fire(transform.forward, body.velocity);
        }
    }
}
