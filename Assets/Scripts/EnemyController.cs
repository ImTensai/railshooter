﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    GunController gun;

    public float minDist = 100f;
    public float fireDelay = 2f;
    public float secondsAhead = 3f;
    public float speed = 12f;

    public bool isFreeRange;

    float detectionRange = 20f;

    private void Awake()
    {
        gun = GetComponentInChildren<GunController>();
    }

    private void Start()
    {
       StartCoroutine(EnemyFire());
    }

    IEnumerator EnemyFire()
    {
        while (enabled)
        {
            yield return new WaitForEndOfFrame();
            Vector3 diff = PlayerController.instance.transform.position - gun.transform.position;
            diff += PlayerController.instance.GetComponent<Rigidbody>().velocity * secondsAhead;
            float d = diff.sqrMagnitude;

            if (d < minDist * minDist)
            {
                gun.transform.forward = diff;
                Debug.Log("Tracking Player");
                gun.EnemyFire();
                Debug.Log("Shooting Player");
                yield return new WaitForSeconds(fireDelay);
            }
        }
    }

    IEnumerator FreeRangeEnemies()
    {
        RaycastHit hit;
        Vector3 raycastOffset = Vector3.zero;

        isFreeRange = true;
        
        // This creates the raycast box
        Vector3 left = transform.position - transform.right * speed;
        Vector3 right = transform.position + transform.right * speed;
        Vector3 up = transform.position + transform.up * speed;
        Vector3 down = transform.position - transform.up * speed;

        //This draws the raycast box
        Debug.DrawRay(left, transform.forward * detectionRange, Color.cyan);
        Debug.DrawRay(right, transform.forward * detectionRange, Color.cyan);
        Debug.DrawRay(up, transform.forward * detectionRange, Color.cyan);
        Debug.DrawRay(down, transform.forward * detectionRange, Color.cyan);

        //tells the enemy not to run into the player
        if (Physics.Raycast(left, transform.forward, out hit, detectionRange))
        {
            raycastOffset += Vector3.right;
        }
        else if (Physics.Raycast(right, transform.forward, out hit, detectionRange))
        {
            raycastOffset -= Vector3.right;
        }

        if (Physics.Raycast(up, transform.forward, out hit, detectionRange))
        {
            raycastOffset -= Vector3.up;
        }
        else if (Physics.Raycast(down, transform.forward, out hit, detectionRange))
        {
            raycastOffset += Vector3.up;
        }

        if(raycastOffset != Vector3.zero)
        {
            transform.Rotate(raycastOffset * 5f * Time.deltaTime);
        }
        yield return new WaitForEndOfFrame();
    }

}
