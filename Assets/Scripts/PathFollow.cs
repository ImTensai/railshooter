﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFollow : MonoBehaviour
{
    Rigidbody body;

    public Transform[] path;
    int index = 0;

    public bool shouldFollow = true;

    public float speed;
    public float maxSpeedChange;

    void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (!shouldFollow || index >= path.Length) return;

        Transform target = path[index];

        Vector3 diff = target.position - transform.position;
        Vector3 dir = diff.normalized;

        Vector3 v = dir * speed - body.velocity;

        if(v.sqrMagnitude > maxSpeedChange * maxSpeedChange)
        {
            v = v.normalized * maxSpeedChange;
        }
        body.AddForce(v, ForceMode.VelocityChange);
        transform.forward = dir;

        if(diff.sqrMagnitude < 5)
        {
            index++;
        }
    }

    private void OnDrawGizmos()
    {
        for(int i = 0; i < path.Length-1; i++)
        {
            Gizmos.DrawLine(path[i].position, path[i + 1].position);
        }
    }
}
