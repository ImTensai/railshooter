﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{
    public static TextController instance;
    public GameObject textBox;
    Text text;

    Queue<IEnumerator> textQueue = new Queue<IEnumerator>();

    private void Awake()
    {
        if (instance == null) instance = this;

        text = textBox.GetComponentInChildren<Text>();
        
    }

    private IEnumerator Start()
    {
        textBox.SetActive(false);
        while(enabled)
        {
            if(textQueue.Count > 0)
            {
                textBox.SetActive(true);
                while(textQueue.Count > 0)
                {
                    yield return StartCoroutine(textQueue.Dequeue());
                }
                textBox.SetActive(false);
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public static void RevealText(string text, float waitTime)
    {
        instance.textQueue.Enqueue(instance.RevealTextCoroutine(text, waitTime));
    }

    IEnumerator RevealTextCoroutine (string text, float delay)
    {
        this.text.text = "";

        for (int i = 0; i <= text.Length; i++)
        {
            this.text.text = text.Substring(0, i);
            yield return new WaitForSeconds(delay);
        }
        yield return new WaitForSeconds(1);
    }
}
