﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageController : MonoBehaviour
{
    public float damage = -10;

    public bool deactivateOnHit = false;

    private void OnTriggerEnter(Collider c)
    {
        Debug.Log("Hit!");
        OnHit(c.gameObject);
    }
   
    private void OnHit(GameObject g)
    {
        ShieldController s = g.GetComponentInChildren<ShieldController>();
        HealthController h = g.GetComponentInParent<HealthController>();

        if (s != null) //Not null means the script is on the object
        {
            return;
        }

        if (h != null) //Not null means the script is on the object
        {
            h.ChangeHealth(damage);
        }

        if (deactivateOnHit)
        {
            gameObject.SetActive(false);
        }
    }
    
}
