﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public static EnemySpawner instance;
    public GameObject freeRangeEnemy;

    public int maxEnemies = 20;
    public int spawnAmount = 1;

    public float interval;

    public bool isFreeRange;
    
    private List<GameObject> enemies;

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    private void Start()
    {
        enemies = new List<GameObject>();

        for (int i = 0; i < maxEnemies; i++)
        {
            GameObject g = Instantiate(freeRangeEnemy);
            g.SetActive(false);
            enemies.Add(g);
        }
    }

    public GameObject Spawn ()
    {
        for (int t = 0; t <= maxEnemies; t++)
        {
            isFreeRange = true;

            GameObject g = enemies.Find(IsInactive);
            Vector3 pos = transform.position + new Vector3(Random.Range(-400f, 400f), Random.Range(-50f, 50f), Random.Range(-400f, 400f));

            if(g != null)
            {
                Debug.Log("Enemy Spawned");
                g.SetActive(true);
                return g;
            }
        }
        Debug.Log("nothing spawned");
        return null;
    }

    bool IsInactive(GameObject g)
    {
        return !g.activeSelf;
    }

    /* public IEnumerator SpawnCoroutine()
   {
       while (enabled)
       {





           yield return new WaitForSeconds(1f);

           for (int i = 0; i < spawnAmount; i++)
           {
               GameObject g = Spawn();
               Debug.Log("spawning enemy");
               if (g != null)
               {
                   g.transform.position = pos;
                   g.SetActive(true);
               }
           }
           yield return new WaitForSeconds(interval);
       }
   }*/
}
