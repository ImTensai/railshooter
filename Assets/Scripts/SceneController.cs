﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public static SceneController sceneController;

    private void Awake()
    {
        sceneController = this;
    }

    public static void LoadStartMenu()
    {
        SceneManager.LoadScene("StartMenu");
    }

    public void StartRailShooter()
    {
        SceneManager.LoadScene("RailShooter");
    }

    public void StartLevel1()
    {
        SceneManager.LoadScene("RailShooter_Level1");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ExitGame();
        }
        
    }
    void ExitGame()
    {
        Application.Quit();
    }
}
