﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RingCounterController : MonoBehaviour
{
    Text ringCounterText;

    private int count = 0;

    private void Awake()
    {
        ringCounterText = GetComponent<Text>();
    }

    private void OnEnable()
    {
        RingController.onHitRing += UpdateCount;
    }

    private void OnDisable()
    {
        RingController.onHitRing -= UpdateCount;
    }

    private void UpdateCount (bool h) 
    {
        if (h)
        {
            count++;
            AudioManager.Play("RingSound");
            ringCounterText.text = count.ToString("000");
        }
        else
        {
            count = 0;
            ringCounterText.text = count.ToString("000");
        }
    }
}
