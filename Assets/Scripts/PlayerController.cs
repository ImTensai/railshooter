﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    Rigidbody body;
    
    Animator anim;
    Vector2 animDir;

    bool isAllrangeMode;

    float lastHardLeft = -1;
    float lastHardRight = -1;

    public float rotateSpeed = 60;

    public float barrelRollWindow = 0.5f;

    public Vector3 speed = new Vector3(10, 10, 15);

    public float maxAcceloration = 10;

    public float hardTurnMultiplier = 3;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        body = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        bool turnHardLeft = Input.GetKey(KeyCode.LeftShift);
        bool turnHardRight = Input.GetKey(KeyCode.RightShift);

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (Time.time - lastHardLeft < barrelRollWindow)
            {
                anim.SetTrigger("barrelLeft");
                AudioManager.Play("BarrelRollSound -Chance");
            }
            lastHardLeft = Time.time;
            lastHardRight = -1;
        }
        else if (Input.GetKeyDown(KeyCode.RightShift))
        {
            if (Time.time - lastHardRight < barrelRollWindow)
            {
                anim.SetTrigger("barrelRight");
                AudioManager.Play("BarrelRollSound -Chance");
            }
            lastHardRight = Time.time;
            lastHardLeft = -1;
        }

        if ((turnHardLeft && x < 0) || (turnHardRight && x > 0)) x *= hardTurnMultiplier;
        animDir = Vector2.Lerp(animDir, new Vector2(x, y), maxAcceloration * Time.deltaTime);

        if (isAllrangeMode)
        {
            transform.Rotate(Vector3.up * x * Time.deltaTime * rotateSpeed);
        }

        Vector3 targetVelocity = transform.right * x * speed.x + Vector3.up * -y * speed.y + transform.forward * speed.z;
        Vector3 velocityChange = targetVelocity - body.velocity;
        Vector2 xyChange = (Vector2)velocityChange;

        if (xyChange.sqrMagnitude > maxAcceloration * maxAcceloration)
        {
            xyChange = xyChange.normalized * maxAcceloration;
            velocityChange.x = xyChange.x;
            velocityChange.y = xyChange.y;
        }

        body.AddForce(velocityChange, ForceMode.VelocityChange);

        anim.SetFloat("x", animDir.x);
        anim.SetFloat("y", animDir.y);
    }

    private void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.CompareTag("AllRange"))
        {
            isAllrangeMode = true;
        }
    }
}
