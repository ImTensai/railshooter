﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextTrigger : MonoBehaviour
{
    public string displaytext = "";
    public float duration = 2;

    private void OnTriggerEnter(Collider c)
    {
        if (c.GetComponentInParent<PlayerController>()== null) return;

        TextController.RevealText(displaytext, 0.05f);
    }
}
