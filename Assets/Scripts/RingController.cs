﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingController : MonoBehaviour
{
    // Place on objects you want control your ring counter

    public delegate void HitRing(bool hit);
    public static event HitRing onHitRing = delegate { };

    public float radius = 8f;
    
    private void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.GetComponentInParent<PlayerController>())
        {
            Debug.Log("Ring Hit");
            bool didHit = (transform.position - c.transform.position).sqrMagnitude < radius * radius;
            onHitRing(didHit);
        }
    }

    private void OnTriggerExit(Collider c)
    {
        if (c.gameObject.CompareTag("Player"))
        {
            gameObject.SetActive(false);
        }
    }
}
