﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float speed = 20;
    public float lifeTime = 4;

    Rigidbody body;

    public void Awake()
    {
        body = GetComponent<Rigidbody>();
    }

    public void Fire (Vector3 dir, Vector3 parentVelocity)
    {
        dir.Normalize();
        body.velocity = dir * speed + parentVelocity;
        StartCoroutine(SelfDestruct());
    }

    IEnumerator SelfDestruct()
    {
        yield return new WaitForSeconds(lifeTime);
        gameObject.SetActive(false);
    }
}
